import { Component, EventEmitter, Input, Output } from '@angular/core';
import { QuestionItem } from '../../../interfaces';

@Component({
  selector: 'app-quiz-item',
  templateUrl: './quiz-item.component.html',
  styleUrls: ['./quiz-item.component.scss'],
})
export class QuizItemComponent {
  @Input('currentQuestionItem') questionItem!: QuestionItem;
  @Output() selectedAnswerId: EventEmitter<number> = new EventEmitter<number>();

  answerClicked(linkedQuestionId: number) {
    this.selectedAnswerId.emit(linkedQuestionId);
  }
}
