import { Injectable } from '@angular/core';
import { QuestionItem } from '../../interfaces';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  constructor() {}
  private availableAnswersImages: string[] = [];

  private quizTreeRecursion(questionItem: QuestionItem) {
    if (questionItem.answerItems.length === 0) {
      this.availableAnswersImages.push(
        `../../../../assets/images/${questionItem.metadata!.imageUrl}`
      );
      return;
    }

    questionItem.answerItems.forEach(({ linkedQuestionId }) =>
      this.quizTreeRecursion(this.getQuestionById(linkedQuestionId))
    );
  }

  public getFirstQuestionId(): number {
    return mockFirstQuestionId;
  }

  public getQuizTree(): QuestionItem[] {
    return mockQuizTree;
  }

  public getQuestionById(id: number): QuestionItem {
    return mockQuizTree.find((item) => item.id === id) || ({} as QuestionItem);
  }

  public getAvailableAnswersImages(currentQuestion: QuestionItem): string[] {
    this.availableAnswersImages = [];

    this.quizTreeRecursion(currentQuestion);

    return [...this.availableAnswersImages];
  }
}

const mockFirstQuestionId = 1;
const mockQuizTree = [
  {
    id: 1,
    text: 'Are you looking for a cheerleader or a roomate that you have to feed?',
    answerItems: [
      { linkedQuestionId: 2, answerText: 'COMPANION' },
      { linkedQuestionId: 3, answerText: 'ROOMMATE' },
    ],
  },
  {
    id: 2,
    text: 'DOG',
    answerItems: [],
    metadata: { imageUrl: 'dog.png' },
  },
  {
    id: 3,
    text: 'Do you mind having to clean up shedded fur?',
    answerItems: [
      { linkedQuestionId: 4, answerText: 'NOPE, THE FUZZIER THE BETTER' },
      { linkedQuestionId: 5, answerText: 'VERY MUCH SO' },
    ],
  },
  {
    id: 4,
    text: "What's your stench comfort level?",
    answerItems: [
      {
        linkedQuestionId: 6,
        answerText: 'I STINK, THEREFORE I AM (OKAY WITH IT).',
      },
      { linkedQuestionId: 7, answerText: 'MINIMAL, PLEASE?' },
    ],
  },
  {
    id: 6,
    text: 'FERRET',
    answerItems: [],
    metadata: { imageUrl: 'ferret.png' },
  },
  {
    id: 7,
    text: 'Would you rather be scratched or bitten?',
    answerItems: [
      { linkedQuestionId: 8, answerText: 'WHY NOT BOTH?' },
      { linkedQuestionId: 9, answerText: 'BITTEN? I GUESS?' },
    ],
  },
  {
    id: 8,
    text: 'CAT',
    answerItems: [],
    metadata: { imageUrl: 'cat.png' },
  },
  {
    id: 9,
    text: 'RABBIT',
    answerItems: [],
    metadata: { imageUrl: 'rabbit.png' },
  },
  {
    id: 5,
    text: 'How many legs can you handle?',
    answerItems: [
      { linkedQuestionId: 10, answerText: '0' },
      { linkedQuestionId: 11, answerText: '2' },
      { linkedQuestionId: 12, answerText: '4' },
      { linkedQuestionId: 13, answerText: 'SO GODDAMN MANY' },
    ],
  },
  {
    id: 11,
    text: 'BIRD',
    answerItems: [],
    metadata: { imageUrl: 'bird.png' },
  },
  {
    id: 13,
    text: 'TARANTULA',
    answerItems: [],
    metadata: { imageUrl: 'tarantula.png' },
  },
  {
    id: 10,
    text: 'How do you feel about buying dead mice every month?',
    answerItems: [
      { linkedQuestionId: 14, answerText: 'COOL WITH IT' },
      { linkedQuestionId: 15, answerText: 'NOOOOT GREAT' },
    ],
  },
  {
    id: 14,
    text: 'SNAKE',
    answerItems: [],
    metadata: {
      suggestion: 'You can call it "Heaven", for the dead mice shake!',
      imageUrl: 'snake.png',
    },
  },
  {
    id: 15,
    text: 'FISH',
    answerItems: [],
    metadata: { imageUrl: 'fish.png' },
  },
  {
    id: 12,
    text: 'hot or cold-blooded?',
    answerItems: [
      { linkedQuestionId: 16, answerText: 'I LIKE IT HOT!' },
      { linkedQuestionId: 17, answerText: 'COLD AS ICE.' },
    ],
  },
  {
    id: 17,
    text: 'Are you in a hurry to get a pet?',
    answerItems: [
      { linkedQuestionId: 18, answerText: 'NAH' },
      { linkedQuestionId: 19, answerText: 'YEAH!' },
    ],
  },
  {
    id: 18,
    text: 'TURTLE',
    answerItems: [],
    metadata: { imageUrl: 'turtle.png' },
  },
  {
    id: 19,
    text: 'LIZARD',
    answerItems: [],
    metadata: { imageUrl: 'lizard.png' },
  },
  {
    id: 16,
    text: 'What is you tail-type preference?',
    answerItems: [
      { linkedQuestionId: 20, answerText: 'NO TAIL' },
      { linkedQuestionId: 21, answerText: 'LONG AND FUZZY' },
      { linkedQuestionId: 22, answerText: 'LIKE A GIANT, DEAD EARTHWORM' },
    ],
  },
  {
    id: 21,
    text: 'GERBIL',
    answerItems: [],
    metadata: { imageUrl: 'gerbil.png' },
  },
  {
    id: 22,
    text: 'RAT',
    answerItems: [],
    metadata: { imageUrl: 'rat.png' },
  },
  {
    id: 20,
    text: 'spikes?',
    answerItems: [
      { linkedQuestionId: 23, answerText: 'YEAH, WHY NOT!' },
      { linkedQuestionId: 24, answerText: 'YIKES!' },
    ],
  },
  {
    id: 23,
    text: 'HEDGEHOG',
    answerItems: [],
    metadata: { imageUrl: 'hedgehog.png' },
  },
  {
    id: 24,
    text: 'HAMSTER',
    answerItems: [],
    metadata: { imageUrl: 'hamster.png' },
  },
];
