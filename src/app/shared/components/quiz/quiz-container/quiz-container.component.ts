import { Component, OnInit } from '@angular/core';
import { QuestionItem } from '../../../interfaces';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-quiz-container',
  templateUrl: './quiz-container.component.html',
  styleUrls: ['./quiz-container.component.scss'],
})
export class QuizContainerComponent implements OnInit {
  public questionItems: QuestionItem[] = [];
  public availableAnswersImages: string[] = [];
  public previousQuestionsIds: number[] = [];
  public currentQuestionId: number = 0;

  get imagesUrls(): string[] {
    return this.quizService.getAvailableAnswersImages(this.currentQuestion);
  }

  get currentQuestion(): QuestionItem {
    return this.quizService.getQuestionById(this.currentQuestionId);
  }

  get hasPreviousSteps(): boolean {
    return this.previousQuestionsIds.length > 0;
  }

  constructor(private quizService: QuizService) {}

  ngOnInit(): void {
    this.currentQuestionId = this.quizService.getFirstQuestionId();
    this.questionItems = this.quizService.getQuizTree()
  }

  answerClicked(selectedAnswerLinkedQuestionId: number) {
    this.previousQuestionsIds.push(this.currentQuestionId);
    this.currentQuestionId = selectedAnswerLinkedQuestionId;
  }

  returnToPrevious() {
    this.currentQuestionId = this.previousQuestionsIds.pop()!;
  }
}
