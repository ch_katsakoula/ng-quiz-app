import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizItemComponent } from './quiz-item/quiz-item.component';
import { QuizContainerComponent } from './quiz-container/quiz-container.component';
import { QuizRemainingResultsComponent } from './quiz-remaining-results/quiz-remaining-results.component';

const COMPONENTS = [
  QuizItemComponent,
  QuizContainerComponent,
  QuizRemainingResultsComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  imports: [CommonModule],
})
export class QuizModule {}
