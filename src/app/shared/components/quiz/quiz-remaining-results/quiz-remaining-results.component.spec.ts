import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizRemainingResultsComponent } from './quiz-remaining-results.component';

describe('QuizRemainingResultsComponent', () => {
  let component: QuizRemainingResultsComponent;
  let fixture: ComponentFixture<QuizRemainingResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizRemainingResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizRemainingResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
