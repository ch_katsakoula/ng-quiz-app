import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-quiz-remaining-results',
  templateUrl: './quiz-remaining-results.component.html',
  styleUrls: ['./quiz-remaining-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuizRemainingResultsComponent {
  @Input('remainingResultsImagesUrls') imagesUrls: string[] = [];
}
