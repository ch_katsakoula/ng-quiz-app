export interface AnswerItem {
  linkedQuestionId: number;
  answerText: string;
}
