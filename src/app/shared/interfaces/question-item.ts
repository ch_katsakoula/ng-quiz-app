import { AnswerItem } from './answer-item';

export interface QuestionItem {
  id: number;
  text: string;
  answerItems: AnswerItem[];
  metadata?: { imageUrl: string };
}
