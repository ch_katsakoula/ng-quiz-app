import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/button';
import { AppComponent } from './app.component';
import { QuizModule } from './shared/components/quiz/quiz.module';
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ButtonModule, QuizModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
